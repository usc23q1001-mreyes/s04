# Mini-Exercise
# 1. Create a car dictionary with the following keys 
# brand, model, year of make, color
# 2. Print the following statement from the details:
# "I own a <brand> <model> and it was made in <year of make>"


car = {
  'brand': 'Rolls-Royce',
  'model': 'Phantom',
  'year_of_make': 2019,
  'color': 'Black'
}


print(f'I own a {car["brand"]} {car["model"]} and it was made in {car["year_of_make"]}')